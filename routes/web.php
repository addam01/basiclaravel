<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/hello',function(){
//     return 'Hello World!';
// });
Route::get('hello', 'Hello@index');
Route::get('/hello2/{name}', 'Hello@show');

Auth::routes();

Route::get('/testList', 'IndexController@index');
Route::get('/testcrud/{id}', 'IndexController@crud');

Route::get('/user', 'UserController@index');
Route::get('/usershow', 'UserController@show');
Route::get('/useredit/{id}', 'UserController@edit');
Route::put('/userupdate/{id}' , 'UserController@update');
Route::get('/usercreate', 'UserController@create');
Route::delete('/userdestroy/{id}', 'UserController@destroy');

Route::get('/about', function(){
  return "About us";
});

Route::get('/contact', function(){
  return "Contact us";
});

Route::get('/home', 'HomeController@index');
