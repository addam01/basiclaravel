# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

# Notes
* For faking individual models and auto migrate
    1. php artisan tinker
    2. factory(App\Model::class,10)->create()
    3. You'll need the model, and ModelFactory in the Seeder folder
* You can Chain methods, but each have their sequence that you'll have to follow
* Step of making a Data schema
  1. php artisan make:Model Post -m
  2. setup migration script schema
  3. setup seeder file
* The ORM way of joining tables
  1. Create 2 models, migrate and seeder
  2. If A 1< B table, create a function name B in class A
  3. add return $this->hasMany('App\\B');
  4. Create a function name A in class B
  5. add return $this->belongsTo('App\\A')
  6. In the controller, just call the functions of the Model to return the values. Just use both Models in the controller.

[Package List](http://packalyst.com/packages/package/cleaniquecoders/artisan-extended)
[Project Git](https://github.com/cleaniquecoders/training-laravel-magic-dec-2016)
[Bootstraping forms](http://bootsnipp.com/forms)