<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class IndexController extends Controller
{
  public function index()
  {
    $users = User::all();
    // To dump array without JSON
    // dd($user);

    // Or just dump into view
    // return User::all();

    //Or if just want to paginate it
    $users = User::paginate(5);
      return view('test', compact('users'));
  }

  public function crud($id){
    // dd($id);
    $user = User::find($id);
    return view('show', compact('user'));
    // return "lol";
  }
}

/**
 *To edit the pagination template, cmd php artisan vendor::publish
 *The folder to edit is in the View -> Vendor -> Pagination
