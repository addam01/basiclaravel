@extends("layouts.app")

@section('content')
  <div class="container">
    {{$users->links()}}
    <table>
      @foreach($users as $user)
        <tr>
             <td>{{ $user->name }}</td>
             <td>{{ $user->email }}</td>
             <td>
               <div class="btn-group">
                 <a class="btn btn-default" href="{{ action('IndexController@crud', $user->id) }}"> SHOW </a>
                 <a class="btn btn-default"> EDIT </a>
                 <a class="btn btn-default"> DELETE </a>
               </div>
             </td>
           </tr>
      @endforeach
    </table>
@endsection
