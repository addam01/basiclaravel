@extends("layouts.app")

@section('content')
  <div class="container">
    {{$users->links()}}
    <table>
      @foreach($users as $user)
        <tr>
             <td>{{ $user->name }}</td>
             <td>{{ $user->email }}</td>
             <td>
               <div class="btn-group">
                 <a class="btn btn-default" href="{{ action('UserController@show', $user->id) }}"> SHOW </a>
                 <a class="btn btn-default" href="{{ action('UserController@edit', $user->id) }}"> EDIT </a>
                 <a href="{{ action('UserController@destroy', $user->id) }}"
									class="btn btn-danger"
								    onclick="event.preventDefault();
								    		if(confirm('Are you sure want to delete the record?')) {
								             document.getElementById('delete-user-{{ $user->id }}').submit();
								    		}">
								    Delete
								</a>

					            <form id="delete-user-{{ $user->id }}"
					            	action="{{ action('UserController@destroy', $user->id) }}"
					            	method="POST"
					            	style="display: none;">
					                {{ csrf_field() }}
					                {{ method_field('DELETE') }}
					            </form>
               </div>
             </td>
           </tr>
      @endforeach
    </table>
@endsection
